public class Algorytms {
    public static void main(String[] args) {
        Task_1();
        //Task_2();
    }

    static void Task_1() {
        System.out.println();

        int rownumber = 4; // выбираем строку, в которой хочем узнать сумму
        assert  rownumber > 0 : "Not valid"; // ассерт

        int oddNumbersInRow = 0; //  oddNumbersInRow - находим количество чисел в трегоульнике в диапазоне [первая линия - заданная линия]
        for (int i = 0; i <= rownumber; i++) {
            oddNumbersInRow += i;
        }
        // СУММА ЧИСЕЛ БОЛЬШОГО ТРЕУГОЛЬНИКА
        int oddSum = 0; // находим сумму всех чисел в диапазоне [первая линии - заданная линия]
        for (int i = 1; i <= 2 * oddNumbersInRow; i += 2) {
            oddSum += i;
        }
        // СУММА ЧИСЕЛ МАЛЕНЬКОГО ТРЕУГОЛЬНИКА
        int oddSumBeforePreviousLine = 0;
        int oddNumbersInPreviousRow = oddNumbersInRow - rownumber; // находим количесво чисел в маленьком треугольнике
        for (int i = 1; i <= 2 * oddNumbersInPreviousRow; i += 2) { // находим сумму чисел в диапазоне [первая линия - линяя ДО заданной линии]
            oddSumBeforePreviousLine += i;
        }
        int triangleLineSum = oddSum - oddSumBeforePreviousLine; // находим разницу между суммой чисел в большом треугольнике и маленьком треугольнике
        System.out.print("Сумма чисел в " + rownumber + "-ой линии треугольника " + triangleLineSum);
    }

    static void Task_2() {
        System.out.print("\n");

        int first = -1;
        int second = 3;

        int sum = 0;

        int min;
        int max;

        if (first > second) {
            min = second;
            max = first;
        } else {
            min = first;
            max = second;
        }

        for (int i = min; i <= max; i++) {
            sum += i;
        }

        System.out.println("Сумма между числами " + first + " и " + second + " равна " + sum);
    }
}

