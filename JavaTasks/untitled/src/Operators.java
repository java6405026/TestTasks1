public class Operators {
    public static void main(String[] args) {
        //Task_1();
        //Task_2();
        //Task_3();
        //Task_4();
        Task_5();
        //Task_6();
        Task_7();
    }

    static void Task_1() {

        System.out.print("\n");

        int firstInt = 1;
        int secondInt = 2;
        int thirdInt = 3;

        int averageInt;

        // Check first
        boolean isFirstIntAverage = (firstInt < secondInt && firstInt > thirdInt) || (firstInt > secondInt && firstInt < thirdInt);
        boolean isSecondIntAverage = (secondInt < firstInt && secondInt > thirdInt) || (secondInt > firstInt && secondInt < thirdInt);
        boolean isThirdIntAverage = (thirdInt < secondInt && thirdInt > firstInt) || (thirdInt > secondInt && thirdInt < firstInt);

        if (isFirstIntAverage) {
            averageInt = firstInt;
        } else if (isSecondIntAverage) {
            averageInt = secondInt;
        } else if (isThirdIntAverage) {
            averageInt = thirdInt;
        } else {
            // Открытй вопрос - как определять среднее число, если хотя бы два числа равные
            // Скорее всего нужно ассерты добавить на то: чтобы числа не было равными
            averageInt = 0;
        }

        System.out.println("averageInt=" + averageInt);
    }

    static void Task_2() {

        System.out.print("\n");

        int value = 15;

        boolean isMultipleOfThree = value % 3 == 0;
        boolean isMultipleOfFive = value % 5 == 0;

        if (isMultipleOfThree && isMultipleOfFive) {
            System.out.println("FizBuz");
        } else if (isMultipleOfThree) {
            System.out.println("Fiz");
        } else if (isMultipleOfFive) {
            System.out.println("Buz");
        }
    }

    static void Task_3() {
        System.out.print("\n");

        //Чтобы треугольник существовал, сумма всегда должна быть больше отдельной стороны или,
        // по крайней мере, не меньше, если учитывать так называемый вырожденный треугольник.
        int firstTriangleLine = 2;
        int secondTriangleLine = 2;
        int thirdTriangleLine = 2;

        if ((firstTriangleLine + secondTriangleLine > thirdTriangleLine) &&
                (firstTriangleLine + thirdTriangleLine > secondTriangleLine) &&
                (secondTriangleLine + thirdTriangleLine > firstTriangleLine)) {
            System.out.println("Треугольник существует");

        } else {
            System.out.println("Треугольник не существует");
        }
    }


    static void Task_4() {

        System.out.print("\n");

        int value = 15;

        boolean isMultipleOfThree = value % 3 == 0;
        boolean isMultipleOfFive = value % 5 == 0;
        boolean isMultipleOfFiveAndThree = isMultipleOfThree && isMultipleOfFive;

        int resultValue = 0;

        if (isMultipleOfThree) {
            resultValue = 1;
        }

        if (isMultipleOfFive) {
            resultValue = 2;
        }

        if (isMultipleOfFiveAndThree) {
            resultValue = 3;
        }

        switch (resultValue) {
            case 1:
                System.out.println("Fiz");
                break;

            case 2:
                System.out.println("Buz");
                break;

            case 3:
                System.out.println("FizBuz");
                break;

            case 0:
                System.out.println("Not multiple of 3 or 5 or both");
                break;
        }
    }

    static void Task_5() {
        System.out.print("\n");

        int a = 1;
        int b = -8;
        int c = 12;  // значения для формулы ниже
        double discriminant = b * b - 4 * a * c; // формула дискриминанта
        double x1 = (-b + Math.sqrt(discriminant)) / (2 * a); //  по формуле находим первый корень x1
        double x2 = (-b - Math.sqrt(discriminant)) / (2 * a); //  по формуле находим второй корень x2
        if (discriminant > 0) {
            System.out.println("x1 = " + x1);
            System.out.println("x2 = " + x2);  // описываем условие, что при положительном дискриминате выводятся 2 корня
        } else if (discriminant == 0) {
            System.out.println("x1 = x2 = " + x1); // в ином случае (дискриминант равен 0) уравнение имеет один корень
        } else {
            System.out.println("Нет корней");// в случае отрицательного дискриминанта не будет корней
        }
    }

    static void Task_6() {
        System.out.print("\n");

        // Решение
        // https://gist.github.com/elenapogoda/a2d8121679bcf4112bdeaac56cadd56f

        int first = 8;
        int second = 632;
        int third = 2;

        if (first < second && first < third && second < third) {
            System.out.println(first + " " + second + " " + third);
        } else if (second < first && second < third && first < third) {
            System.out.println(second + " " + first + " " + third);
        } else if (third < first && third < second && first < second) {
            System.out.println(third + " " + first + " " + second);
        }
    }

    static void Task_7() {
        System.out.print("\n");
        int secondsLeft = (int) (Math.random() * 28800); // Math.random это функция, которая возвращает рандомные значения, задаем ей диапазон, так как по дефолту он от 0 до 1
        assert secondsLeft >= 0 : "Время не может быть установлено"; //ассерты
        assert secondsLeft < 28800 : "Время не может быть установлено";
        System.out.println("Осталось секунд: " + secondsLeft); // здесь будет находится значение в секундах, которое берется из рандома (для Федорова)
        int hoursLeft = secondsLeft / 3600; // перевод из секунд в часы
        if (secondsLeft == 0) {
            System.out.println("Рабочий день закончен"); // условие, если осталось 0 секунд
        } else if (hoursLeft > 0) {
            System.out.println("Осталось часов: " + hoursLeft); // условие, в котором отображются полные часы (для сотрудниц)
        } else {
            System.out.println("Осталось менее часа"); // если предыдущие 2 условия не сработали
        }
    }
}
