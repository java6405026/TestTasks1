public class Loops {

    public static void main(String[] args) {
        //  Task_1();
//        Task_2();
Task_3();
//        Task_4();
//        Task_5();
//        Task_6();
//        Task_7();
//        Task_8();
//        Task_9();
    }

    static void Task_1() {
        System.out.print("\n");
        String str = "QQWxxWQQ"; // объявляем строку

        boolean answer = true;
        for (int i = 0; i < str.length() / 2; i++) { // проверяем слово до середины цикла

            char beginChar = str.charAt(i); // берем символ

            char endChar = str.charAt(str.length() - i - 1); // берем зеркальный символ в конце

            if (beginChar != endChar){ // условие, при котором символ из начала не равен циклу из конца (будет ответ false)
                answer = false;
                break;
            }
        }

        System.out.println("Строка " + str + " Палиндром: " + answer);
    }

    static void Task_2() {
        System.out.print("\n");

        int min = 0;
        int max = 15;
        int step = 2;

        assert step > 0 : " Not valid";
        assert min < max : " Not valid";

        for (int i = min; i <= max; i += step) {
            System.out.println(i + " ");
        }
    }

    static void Task_3() {
        System.out.println();

        int value = 6785;
        assert value >= 0 : "Invalid data";

        System.out.println("Каждая цифра, начиная с последней:");

        int rankDivider = 10;
        while (value != 0) {
            int modValue = value % rankDivider;
            value = value / rankDivider;

            System.out.println(modValue);
        }
    }

    static void Task_4() {
        System.out.print("\n");

        int value = 78911;
        assert value >= 0 : "Invalid data";

        String valueString = Integer.toString(value);
        System.out.println("Каждая цифра, начиная с последней:");

        int numbersSum = 0;
        for (int i = valueString.length() - 1; i >= 0; i--) {
            System.out.println(valueString.charAt(i));
            numbersSum += Character.getNumericValue(valueString.charAt(i));
        }

        System.out.println("Сумма цифр в числе=" + numbersSum);
    }

    static void Task_5() {
        System.out.print("\n");

        // таблица умножения
        // нагуглила такую  https://lh5.googleusercontent.com/idzFS071Z10yySfLFUsuOnmuvroOJGI56PHfDJhsOPzGtcQ3ywDrONSy_P0Raut1bdr_eyzyt499LbdFcyo6rDzkhfLvMk-TLKx_vHmxgVBDLPyBGouHEtN5zRLxaqNgOINGc80T

        int min = 1;
        int max = 9;

        for (int i = min; i <= max; i++) {
            for (int j = min; j <= max; j++) {
                System.out.println(i + " умножить на " + j + " равно " + i * j);
            }
        }

        //  и такую https://ic.pics.livejournal.com/tromentano/21702491/26727/26727_original.jpg

        System.out.println();
        System.out.print("  |   ");

        for (int i = min; i <= max; i++) {
            System.out.print(i + "   ");
        }
        System.out.println();
        System.out.println("------------------------------------------");

        for (int i = min; i <= max; i++) {
            System.out.print(i + " |");

            for (int j = min; j <= max; j++) {
                int multiply = i * j;

                if (multiply < 10){ // если однозначное число, то пишем его с дополнительным пробелом, чтобы выровнять таблицу
                    System.out.print("   " + i * j);
                }
                else{
                    System.out.print("  " + i * j);
                }
            }
            System.out.println();
        }
    }

    static void Task_6() {
        System.out.print("\n");

        int childrenCount = 2;

        assert childrenCount > 0 : " Not valid";
        int totalApplesCount = 0;

        for (int i = 1; i <= childrenCount; i++) {
            totalApplesCount += i;
        }

        System.out.println("Всего яблок равно " + totalApplesCount);
    }

    static void Task_7() {
        System.out.print("\n");

        // https://www.baeldung.com/java-calculate-factorial

        int value = 3;

        assert value > 0 : " Not valid";

        long fact = 1;
        for (int i = 2; i <= value; i++) {
            fact = fact * i;
        }

        System.out.println("Факториал числа " + value + " равно " + fact);
    }

    static void Task_8() {

        System.out.print("\n");

        String firstString = "1234567890";
        String secondString = "234";

        String uniqueCharacters = "";


        for (int i = 0; i < firstString.length(); i++) {
            char firstChar = firstString.charAt(i);
            Boolean isFirstCharExistsInSecondString = false;

            for (int j = 0; j < secondString.length(); j++) {

                if (firstChar == secondString.charAt(j)) {
                    isFirstCharExistsInSecondString = true;
                }
            }

            if (!isFirstCharExistsInSecondString) {
                uniqueCharacters += firstChar;
            }
        }

        System.out.println("символы из первой строки, которых нет во второй: " + uniqueCharacters);
    }

    static void Task_9() {

        System.out.print("\n");

        // Рисовать квадрат
        int squareSize = 4;
        assert  squareSize > 0 : "Not Valid";

        System.out.print("Квадрат:\n");

        for (int i = 1; i <= squareSize; i++) {
            for (int j = 1; j <= squareSize; j++) {
                System.out.print("*");
            }
            System.out.print("\n");

        }
        System.out.print("\n");
        System.out.print("\n");

        // Рисовать Треугольник
        System.out.print("Треугольник:\n");

        int size = 5;

        assert  size > 0 : "Not Valid";
        for (int i = 1; i <= size; i++) {
            for (int j = 1; j <= size - i; j++) {
                System.out.print(" ");
            }
            for (int k = 1; k <= 2 * i - 1; k++) {
                System.out.print("*");
            }
            System.out.print("\n");
        }
    }
}


