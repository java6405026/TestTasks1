public class Primitives {

    //FOR TASK 1 ============================================================
    static int firstInt;
    static byte firstByte;
    static short firstShort;
    static long firstLong;
    static float firstFloat;
    static char firstChar;
    static boolean firstBoolean;
    static double firstDouble;
    //============================================================
//FOR TASK 11 ============================================================
    static Integer firstIntWrapper;
    static Byte firstByteWrapper;
    static Short firstShortWrapper;
    static Long firstLongWrapper;
    static Float firstFloatWrapper;
    static Character firstCharWrapper;
    static Boolean firstBooleanWrapper;
    static Double firstDoubleWrapper;
    //============================================================

    public static void main(String[] args) {
        //Task_1();
        //Task_2();
        //Task_3();
        // Task_4();
        //Task_5();
        //Task_6();
        //Task_7();
        //Task_8();
        //Task_9();
        //Task_10();
        //Task_11();
    }
    static void Task_1()
    {
        System.out.print("\n");

        int secondInt = 7;
        byte secondByte = 127;
        short secondShort = 7;
        long secondLong = 7;
        float secondFloat = 7.0f;
        double secondDouble = 7.0d;
        boolean secondBoolean = true;
        char secondChar = 'V';

        System.out.print("Int first=" + firstInt);
        System.out.print(" Int second=" + secondInt + "\n");

        System.out.print("byte first=" + firstByte);
        System.out.print(" byte second=" + secondByte + "\n");

        System.out.print("short first=" + firstShort);
        System.out.print(" short second=" + secondShort + "\n");

        System.out.print("long first=" + firstLong);
        System.out.print(" long second=" + secondLong + "\n");

        System.out.print("float first=" + firstFloat);
        System.out.print(" float second=" + secondFloat + "\n");

        System.out.print("double first=" + firstDouble);
        System.out.print(" double second=" + secondDouble + "\n");

        System.out.print("boolean first=" + firstBoolean);
        System.out.print(" boolean second=" + secondBoolean + "\n");

        System.out.print("char first=" + firstChar);
        System.out.print(" char second=" + secondChar + "\n");
    }

    static  void Task_2() {
        int firstInt = 10;
        byte firstByte = 10;
        short firstShort = 12;
        long firstLong = 20;
        float firstFloat = 33.0f;
        char firstChar = 'V';
        boolean firstBoolean = true;
        double firstDouble = 100.0d;

        System.out.print(" SUM int and double=" + (firstInt + firstDouble) + "\n");
        System.out.print(" SUM double and float=" + (firstDouble + firstFloat) + "\n");
        System.out.print(" SUM float and byte=" + (firstFloat + firstByte) + "\n");
        System.out.print(" SUM char and double=" + (firstChar + firstDouble) + "\n");
        System.out.print(" SUM long and short=" + (firstLong + firstShort) + "\n");

        // Boolean Нельзя складывать с другими типами.
        // Чтобы проверить результат с char можно использовать данную таблицу https://upload.wikimedia.org/wikipedia/commons/1/1b/ASCII-Table-wide.svg
    }
    static  void Task_3() {
        System.out.print("\n");

        byte firstByte = -127;
        System.out.print("firstByte initial=" + firstByte + "\n");
        firstByte--;
        System.out.print("firstByte after decrement=" + firstByte + "\n");
        firstByte++;
        System.out.print("firstByte after increment=" + firstByte + "\n");

        // https://stackoverflow.com/questions/62761132/cannot-assign-128-to-byte-why-does-it-error-out-instead-of-overflowing
        // Byte может принимать значения только между -128 и 127: поэтому byte secondByte = 128; выдаст ошибку, НО можно прописать в скобках, то есть явно назначить тип
        //
        byte secondByte = (byte) 128;
        System.out.print("secondByte initial=" + secondByte + "\n");
        secondByte--;
        System.out.print("secondByte after decrement=" + secondByte + "\n"); // если (-128 - 1) - он выйдет за границы и примет значение 127
        secondByte++;
        System.out.print("secondByte after increment=" + secondByte + "\n");
    }

    static  void Task_4() {
        System.out.print("\n");

        int a = 100;
        int b = a; // 100

        // В зависимости от выбраного действия результаты будут разные.
        // На то, изменится ли значение влияет приоритет операций

        // b = a++; // 100
        // b = ++a; // 101
        // b = a--; // 100
        // b = --a; // 99

        System.out.print(" TASK 4 " +"\n");
        System.out.print("b=" + b + "\n");
    }

    static  void Task_5() {
        System.out.print("\n");

        int a = 3;
        int b = 3;

        a = a++ + ++a;
        b = --b - b--;

        System.out.print(" TASK 5 " +"\n");
        System.out.print("a=" + a + "\n");
        System.out.print("b=" + b + "\n");
    }

    static  void Task_6() {
        int result1;
        double result2;
        double result3;
        int a = 7;
        double b = 3;

        // Такое не сработает!!! Будет ошибка компиляции: result1 = a/b;
        result1 = (int) (a / b);
        result2 = a / b;
        result3 = a % b;

        System.out.print(" TASK 6 " + "\n");
        System.out.print("result1=" + result1 + "\n");
        System.out.print("result2=" + result2 + "\n");
        System.out.print("result3=" + result3 + "\n");
    }

    static  void Task_7() {
        System.out.print("\n");

        int x = 2;
        int y = 5;
        int result = y * 3 + 20 / 2 * x--;

        System.out.print(" TASK 7 " + "\n");
        System.out.print("result=" + result + "\n");

        // Если имеется ввиду порядок вызовов, то будет сначала
        // 1. (y * 3) = 15
        // 2. (20 / 2) = 10
        // 3. 10 * x (при этом x=2) = 20
        // 4. 15 + 20 = 35
        // 5. x--. x = 1 Не влияет на результат
    }

    static  void Task_8() {
        System.out.print("\n");

        int a = 1;
        double b = 2;
        char c = '3';

        System.out.println(a < b);
        System.out.println(b > c);
        System.out.println(a >= c);
        System.out.println(a == b);
        System.out.println(b != c);
        System.out.println(a <= b && b < c);
        System.out.println(a >= b && b < c);
        System.out.println(a <= b || b < c);
        System.out.println(a >= b || b < c);
        System.out.println(a <= b && b < c || c < a);
        System.out.println(a <= b || b < c && c < a);
    }

    static void Task_9() {
        System.out.print("\n");

        int x = -15;

        // Остаток от деления на два должен быть равен 0
        System.out.println("Четное:" + (x % 2 == 0));

        // Больше нуля
        System.out.println("Больше нуля:" + (x > 0));

        // Остаток от деления на 3 и 5 должен быть равен 0
        System.out.println("Одновременно кратен 3 и 5:" + ((x % 3 == 0) && (x % 5 == 0)));
    }

    static void Task_10() {

        System.out.print("\n");

        // Готовое решение https://dvsemenov.ru/kak-vychislit-yavlyaetsya-li-god-visokosnym-v-java/

        int year = 1988;

        assert (year > 0) : "Not valid";

        boolean isVisokosnii = (year % 400 == 0) || (year % 4 == 0 && year % 100 != 0);
        System.out.println("Год: " + year + "Високосный " + isVisokosnii);
    }

    // TASK 11

    static void Task_11() {
        System.out.print("\n");

        // https://javarush.com/groups/posts/1948-objertki-raspakovka-i-zapakovka
        Integer intWrapper = 682;
        Byte byteWrapper = 10;
        Short shortWrapper = 10;
        Long longWrapper = 10L;
        Float floatWrapper = 10.0f;
        Character charWrapper = 'V';
        Boolean booleanWrapper = true;
        Double doubleWrapper = 42.23d;

        System.out.print("Int first=" + firstIntWrapper);
        System.out.print(" Int second=" + intWrapper + "\n");

        System.out.print("byte first=" + firstByteWrapper);
        System.out.print(" byte second=" + byteWrapper + "\n");

        System.out.print("short first=" + firstShortWrapper);
        System.out.print(" short second=" + shortWrapper + "\n");

        System.out.print("long first=" + firstLongWrapper);
        System.out.print(" long second=" + longWrapper + "\n");

        System.out.print("float first=" + firstFloatWrapper);
        System.out.print(" float second=" + floatWrapper + "\n");

        System.out.print("double first=" + firstDoubleWrapper);
        System.out.print(" double second=" + doubleWrapper + "\n");

        System.out.print("boolean first=" + firstBooleanWrapper);
        System.out.print(" boolean second=" + booleanWrapper + "\n");

        System.out.print("char first=" + firstCharWrapper);
        System.out.print(" char second=" + charWrapper + "\n");
    }
}
