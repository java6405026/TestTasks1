public class Arrays {
    public static void main(String[] args) {
        Task_1();
        Task_2();
        Task_3();

//        System.out.println(isVisokosnyi(1996));
//
//        System.out.println(isVisokosnyi(1997));
//
//
//        System.out.println(isVisokosnyi(2000));
//
//        System.out.println(isVisokosnyi(1900));
    }


    static boolean isVisokosnyi(int year){
        boolean result = year % 4 == 0 && !(year % 100 == 0 && year % 400 != 0);
        return result;
    }

    static void Task_1() {
        System.out.print("\n");

        int[] firstArray = {0, 2, 44, 100};
        int[] secondArray = {1, 1, 1, 1};

        assert firstArray.length == secondArray.length : "Not valid";

        int[] thirdArray = new int[firstArray.length];

        assert thirdArray.length == firstArray.length : "Not valid";
        assert thirdArray.length == secondArray.length : "Not valid";

        System.out.println("Добавили числа из первого массива во второй и получили:");

        for (int i = 0; i < thirdArray.length; i++) {
            int sum = firstArray[i] + secondArray[i];
            thirdArray[i] = sum;

            System.out.print(thirdArray[i] + " ");
        }
    }

    static void Task_2() {
        System.out.print("\n");

        int[] array = {1, 66, 244, 23,3,1,2};

        assert array.length > 0 : "Not valid";

        System.out.println("Исходный массив:" );
        for (int element : array) {
            System.out.print(element + " ");
        }

        int[] reversedArray = new int[array.length];


//        for (int i = 0; i < array.length; i++) { // через for с созданием нового массива
//            reversedArray[i] = array[array.length - i - 1];
//        }

        // через foreach с созданием нового массива
        int iterator1 = 0;

        for (int element : array) {
            reversedArray[array.length - iterator1 - 1] = element;
            iterator1++;
        }

        System.out.println();
        System.out.println("Перевернутый массив с созданием нового массива:" );
        for (int element : reversedArray) {
            System.out.print(element + " ");
        }

        int iterator2 = 0;
        for (int element : array) {
            if (iterator2 >= array.length / 2) {
                break;
            }

            int temp = element;
            int indexFromEnd = array.length - iterator2 - 1;
            array[iterator2] = array[indexFromEnd];
            array[indexFromEnd] = temp;

            iterator2++;
        }

        System.out.println();
        System.out.println("Перевернутый массив без создания нового массива:" );

        for (int element : array) {
            System.out.print(element + " ");
        }

        System.out.println();
    }
    static void Task_3()
    {
        System.out.print("\n");

        int[] array = {1, 3, 2, 22, -100};

        System.out.print("Максимальное значение массива " + getMax(array));
        System.out.println();
        System.out.print("Минимальное значение массива " + getMin(array));
    }

    private static int getMax(int[] numbers) {
        assert numbers.length > 0 : "Not valid";
        int maximum = numbers[0];

        for (int i = 0; i < numbers.length; i++) {
            int number = numbers[i];
            if (number > maximum) {
                maximum = number;
            }
        }

        return maximum;
    }

    private static int getMin(int[] numbers) {
        assert numbers.length > 0 : "Not valid";
        int minimum = numbers[0];

        for (int i = 0; i < numbers.length; i++) {
            int number = numbers[i];

            if (number < minimum) {
                minimum = number;
            }
        }

        return minimum;
    }

}
